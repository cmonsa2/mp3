// Load required packages
var mongoose = require('mongoose');

// Define our user schema
var UserSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    pendingTasks: [String],
    dateCreated: { type: Date, default: Date.now }
}, {versionKey: false});

// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);

//Used this to setup assignment: https://uiuc-web-programming.gitlab.io/sp20/slides/devlab_mp3.pdf
/*
 * Connect all of your endpoints together here.
 */
module.exports = function (app, router) {
    app.use('/api', require('./home.js')(router));
    app.use('/api/users', require('./users'));
    app.use('/api/tasks', require('./tasks'));
};

//Used this to setup assignment: https://uiuc-web-programming.gitlab.io/sp20/slides/devlab_mp3.pdf
//Used this tutorial to complete assignment: https://rahmanfadhil.com/express-rest-api/
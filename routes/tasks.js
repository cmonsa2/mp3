var express = require("express"),
  router = express.Router(),
  tasks = require("../models/task");
mongoose = require("mongoose");

router.post("/", function (req, res) {
  if (req.body.name && req.body.deadline) {
    const task = new tasks({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      description: req.body.description || "",
      deadline: req.body.deadline,
      completed: req.body.completed || false,
      assignedUser: req.body.assignedUser || "",
      assignedUserName: req.body.assignedUserName || "unassigned",
    });
    res.status(200).send({
      message: "aight",
      data: task,
    });
  }
});

router.get("/", function (req, res) {
  const user = users.find();
  res.status(200).send({
    message: "OK3",
    data: user,
  });
});

router.get("/:id", function (req, res) {
  users.findOne({ _id: req.params.id }, function (err, foundTask) {
    if (err) {
      res.status(404).send({
        message: "Error 404 can not finds",
        data: {},
      });
    } else {
      res.status(200).send({
        message: "aight we finds",
        data: foundTask,
      });
    }
  });
});

router.put("/:id", function (req, res) {
  if (req.body.name && req.body.deadline) {
    users.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      function (err, searchedUser) {
        if (err) {
          res.status(404).send({
            message: "User with id not found",
            data: {},
          });
        } else {
          res.status(200).send({
            message: "You did it! :)",
            data: searchedUser,
          });
        }
      }
    );
  } else {
    res.status(404).send({
      message: "You are missing name and/or deadline >:( Fix it, or else...",
      data: {},
    });
  }
});

router.delete("/:id", function (req, res) {
  res.send("delete rekt task id");
});

module.exports = router;
